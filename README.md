# Introduction to Linked Data - Lecture Material

This lecture provides a crash course on Linked Data, and covers the theory underlying the RDF data model, the creation of Linked Data by hand and by conversion from CSV, and the use of SPARQL to query one's data. 
